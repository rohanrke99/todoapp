package com.todo.task.data.repository.todo

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.paging.DataSource
import com.todo.task.data.datasource.ToDoDataSource
import com.todo.task.data.mapper.ToDoDataMapper
import com.todo.task.data.remote.ToDoService
import com.todo.task.domain.entity.ToDoItem
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.mockk
import io.reactivex.Single
import org.hamcrest.CoreMatchers
import org.junit.Assert.*
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Rule
import org.junit.Test


class ToDoRepoTest{

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    @MockK
    lateinit var dataSource : ToDoDataSource
    @MockK
    lateinit var service:ToDoService
    @MockK
    lateinit var mapper : ToDoDataMapper

    @Before
    @Throws(Exception::class)
    fun setUp(){
        MockKAnnotations.init(this)
        dataSource =  mockk<ToDoDataSource>()
        service = mockk<ToDoService>()
        mapper = mockk<ToDoDataMapper>()


    }

    @Test
    fun testNotNull(){
        assertThat(dataSource, CoreMatchers.notNullValue())
        assertThat(service, CoreMatchers.notNullValue())
        assertThat(mapper, CoreMatchers.notNullValue())
    }

    @Test
    fun testGetData(){
        val repo = ToDoRepo(service = service,
            mapper = mapper,dataSource = dataSource)
        assertThat(repo, CoreMatchers.notNullValue())
        val factory: DataSource.Factory<Int, ToDoItem> = mockk()
        every {repo.get()}.returns(factory)
        var resultItem = repo.get()
        assertTrue(resultItem==factory)

    }


    @Test
    fun testSaveData(){
        val item = createToDoItem()
        val repo = ToDoRepo(service = service,
            mapper = mapper,dataSource = dataSource)
        assertThat(repo, CoreMatchers.notNullValue())
        val singleItem: Single<ToDoItem> = mockk()

        every { repo.save(item) } answers {doNothing()}
        every{repo.getById(any())} answers  {singleItem}
        var resultItem = repo.getById(item.id)
        assertTrue(resultItem==singleItem)


    }

    private fun doNothing() {}

    private fun createToDoItem(): ToDoItem {
        return ToDoItem(
            userId = 1,id = 1,
            title = "delectus aut autem",
            completed = false)
    }

}