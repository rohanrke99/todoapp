package com.todo.task.domain.entity

import org.junit.Assert.*
import org.junit.Test


class ToDoItemTest{

    @Test
    fun testToDoItem(){

        val item = ToDoItem(
           id = 1,  userId = 1,
            title = "delectus aut autem",
            completed = false)
        assertTrue(item.title=="delectus aut autem")
        assertTrue(item.id ==1)
        assertTrue(item.userId==1)
        assertFalse(item.completed)

    }
}