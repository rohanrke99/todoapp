package com.todo.task.domain.usecase.todo


import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.paging.DataSource
import com.todo.task.data.repository.todo.ToDoRepo
import com.todo.task.domain.entity.ToDoItem
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.mockk
import io.reactivex.Single
import org.hamcrest.CoreMatchers
import org.junit.Assert.*
import org.junit.Before

import org.junit.Rule
import org.junit.Test


class ToDoUseCaseTest{

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    @MockK
    lateinit var repo:ToDoRepo

    @Before
    fun init(){
        MockKAnnotations.init(this, relaxUnitFun = true)
        repo =  mockk<ToDoRepo>()
    }

    @Test
    fun testNotNull(){
        assertThat(repo, CoreMatchers.notNullValue())
    }

    @Test
    fun testGetItemList(){
        val useCase = ToDoUseCase(repo)
        assertThat(useCase, CoreMatchers.notNullValue())
        val factory: DataSource.Factory<Int,ToDoItem> = mockk()
        every { useCase.getItemList()} returns (factory)
        var resultItem = useCase.getItemList()
        assertTrue(resultItem==factory)
    }

    @Test
    fun testGetSingleItem(){
        val useCase = ToDoUseCase(repo)
        assertThat(useCase, CoreMatchers.notNullValue())
        val singleItem: Single<ToDoItem> = mockk()
        every { useCase.getSingleItem(any())} returns (singleItem)
        var resultItem = useCase.getSingleItem(1)
        assertTrue(resultItem==singleItem)
    }

    @Test
    fun testGetCount(){
        val useCase = ToDoUseCase(repo)
        assertThat(useCase, CoreMatchers.notNullValue())
        val singleItem: Single<Int> = mockk()
        every {useCase.getCount()}returns (singleItem)
        var resultItem = useCase.getCount()
        assertTrue(resultItem==singleItem)
    }
}