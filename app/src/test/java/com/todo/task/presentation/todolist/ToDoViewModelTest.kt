package com.todo.task.presentation.todolist

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.order.delivery.utils.NetworkUtils
import com.todo.task.data.repository.State
import com.todo.task.domain.entity.ToDoItem
import com.todo.task.domain.usecase.BaseUseCase
import com.todo.task.domain.usecase.todo.ToDoUseCase
import com.todo.task.rx.RxImmediateSchedulerRule
import io.mockk.*
import io.mockk.impl.annotations.MockK
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import org.junit.Before
import org.junit.Rule
import org.junit.Test

import java.net.UnknownHostException

class ToDoViewModelTest{
    @get:Rule
    var rxJavaTestHooksResetRule = RxImmediateSchedulerRule()
    @get:Rule
    val taskExecutorRule = InstantTaskExecutorRule()

    @MockK
    lateinit var useCase: BaseUseCase<ToDoItem, Int>
    @MockK
    lateinit var boundaryCallback: ToDoBoundaryCallback

    private lateinit var viewModel: ToDoViewModel

    @MockK
    lateinit var networkUtils: NetworkUtils

    @Before
    fun setUp() {
        MockKAnnotations.init(this, relaxUnitFun = true)
        useCase = mockk<ToDoUseCase>()
        networkUtils = mockk<NetworkUtils>()
        boundaryCallback = ToDoBoundaryCallback(useCase, CompositeDisposable(),networkUtils = networkUtils)
        every {useCase.getItemList() } returns  (mockk())
        viewModel = ToDoViewModel(useCase = useCase as ToDoUseCase,networkUtils = networkUtils)
        viewModel.boundaryCallback = boundaryCallback
    }

    @Test
    fun toDoListTest() {
        every {useCase.getItemList()} returns (mockk())
        every { useCase.removeAll() } answers {nothing}
        viewModel = ToDoViewModel(useCase = useCase as ToDoUseCase,networkUtils = networkUtils)

        assert(viewModel.itemList.value?.size != 0)
        viewModel.boundaryCallback.state.value?.let { assert(it == State.DONE) }
    }

    @Test(expected = Exception::class)
    fun apiFailTest() {
        every {useCase.fetchItemList(any(),any())} throws (Exception())
        boundaryCallback.onZeroItemsLoaded()
    }
}