package com.todo.task.presentation.todolist

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.order.delivery.utils.NetworkUtils
import com.todo.task.BuildConfig
import com.todo.task.domain.usecase.todo.ToDoUseCase
import com.todo.task.rx.RxImmediateSchedulerRule
import io.mockk.*
import io.mockk.impl.annotations.MockK
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable

import org.junit.Before
import org.junit.Rule
import org.junit.Test

class ToDoBoundaryCallbackTest{

    @get:Rule
    var rxJavaTestHooksResetRule = RxImmediateSchedulerRule()
    @get:Rule
    val taskExecutorRule = InstantTaskExecutorRule()

    @MockK
    lateinit var useCase: ToDoUseCase
    lateinit var boundaryCallback: ToDoBoundaryCallback

    @MockK
    lateinit var networkUtils: NetworkUtils


    @Before
    fun setUp() {
        MockKAnnotations.init(this, relaxUnitFun = true)
        useCase = mockk<ToDoUseCase>()
        networkUtils = mockk<NetworkUtils>()
        boundaryCallback = ToDoBoundaryCallback(useCase, CompositeDisposable(),networkUtils)
    }

    @Test
    fun itemAtFrontLoadTest() {
        every { networkUtils.isInternetAvailable() } returns true
        every { useCase.fetchItemList(any(),any())}returns (Observable.just(mockk()))

        val spy = spyk(boundaryCallback)
        spy.onZeroItemsLoaded()
        verify(exactly = 1) {spy.fetchFromNetwork(0, BuildConfig.PAGE_SIZE)}
        verify(exactly = 1) {
            spy.updateState(any()) }
    }


    @Test
    fun refreshItemsTest() {
        every { networkUtils.isInternetAvailable() } returns true
        every { useCase.fetchItemList(any(), any())} returns (Observable.just(mockk()))

        val spy = spyk(boundaryCallback)
        spy.onRefresh()
        assert(spy.totalCount == 0)
        verify(exactly = 1) {spy.onZeroItemsLoaded()}
        verify(exactly = 1) {spy.fetchFromNetwork(0, BuildConfig.PAGE_SIZE)}
        verify(exactly = 1) {spy.updateState(any())}

    }
}