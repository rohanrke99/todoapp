package com.todo.task.presentation.todolist

import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.todo.task.BR
import com.todo.task.R
import com.todo.task.data.repository.State
import com.todo.task.databinding.ActivityTodoListBinding
import com.todo.task.presentation.BaseActivity
import com.todo.task.utils.BottomDialogListener
import com.todo.task.utils.BottomDialogType
import javax.inject.Inject

class ToDoListActivity : BaseActivity<ActivityTodoListBinding, ToDoViewModel>(),BottomDialogListener {


    private lateinit var viewModel: ToDoViewModel
    @Inject
    lateinit var factory: ViewModelProvider.Factory
    lateinit var adapter: ToDoListAdapter
    private lateinit var binding: ActivityTodoListBinding

    override fun getBindingVariable(): Int {
        return BR.viewModel
    }

    override fun getLayoutId(): Int {
      return R.layout.activity_todo_list
    }

    override fun getViewModel(): ToDoViewModel {
        viewModel = ViewModelProviders.of(this, factory)
            .get(ToDoViewModel::class.java)
        return viewModel
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = getViewDataBinding()
        init()
        setUpObserver()
    }

    override fun setUpObserver() {

        viewModel.itemList.observe(this, Observer {
            adapter.submitList(it)
            adapter.notifyDataSetChanged()
            if (it != null && it.size > 0 && it[0] != null) {
                viewModel.item.set(it[0])
                viewModel.noData.set(false)
            }else{
                viewModel.noData.set(true)
            }
            if (binding.swipeRefresh.isRefreshing) {
                binding.swipeRefresh.isRefreshing = false
            }
        })

        viewModel.boundaryCallback.state.observe(this, Observer {
            when (it) {
                State.ERROR, State.NETWORK_ERROR -> {
                    viewModel.isLoading.set(it == State.DONE)

                    showErrorDialog(
                        if (it == State.NETWORK_ERROR) {
                            R.string.network_error
                        } else R.string.todo_list_error_message
                    )
                    if (binding.swipeRefresh.isRefreshing) {
                        binding.swipeRefresh.isRefreshing = false
                    }


                    //  adapter.notifyItemChanged(adapter.itemCount - 1)
                }
                State.PAGE_LOADING -> {


                    //  adapter.notifyItemChanged(adapter.itemCount - 1)
                }
                State.LOADED -> {
                    viewModel.isLoading.set(it == State.LOADING)
                    showSuccessDialog(R.string.all_data_fetched)
                }
                else -> {
                    viewModel.isLoading.set(it == State.LOADING)
                }
            }
        })
    }

    override fun showSuccessDialog(message: Int) {
          openDialog(
            BottomDialogType.ALL_DATA_FETCHED,
            getString(message), this
        )
    }

    override fun showErrorDialog(message: Int) {
        openDialog(
            BottomDialogType.ERROR_IN_FETCHING,
            getString(message), this
        )
    }

    private fun init() {
        adapter = ToDoListAdapter()
        binding.recyclerView.layoutManager = LinearLayoutManager(this)
        binding.recyclerView.adapter = adapter
        binding.recyclerView.addItemDecoration(
            DividerItemDecoration(
                this, DividerItemDecoration.VERTICAL
            )
        )
        binding.swipeRefresh.setOnRefreshListener {
            viewModel.onRefresh()
        }

        binding.swipeRefresh.setColorSchemeResources(
            android.R.color.holo_blue_bright,
            android.R.color.holo_green_light,
            android.R.color.holo_orange_light,
            android.R.color.holo_red_light
        )
    }

    override fun onDialogClickListener(retry: Boolean) {
        if (retry) {
            viewModel.retry()
        }
    }
}