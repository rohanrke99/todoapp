package com.todo.task.presentation.todolist

import androidx.lifecycle.MutableLiveData
import androidx.paging.PagedList
import com.order.delivery.utils.NetworkUtils
import com.todo.task.BuildConfig
import com.todo.task.data.repository.State
import com.todo.task.domain.entity.ToDoItem
import com.todo.task.domain.usecase.BaseUseCase
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.net.UnknownHostException
import com.todo.task.data.Result

class ToDoBoundaryCallback (private val useCase: BaseUseCase<ToDoItem, Int>,
                            private var disposable: CompositeDisposable,
                            private val networkUtils: NetworkUtils
       ) : PagedList.BoundaryCallback<ToDoItem>(){


    var totalCount: Int = 0
    private var isLoaded: Boolean = false
    private var loadingFirstPage: Boolean = false
    var state: MutableLiveData<String> = MutableLiveData()

    override fun onZeroItemsLoaded() {
        loadingFirstPage = true
        fetchFromNetwork(0, BuildConfig.PAGE_SIZE)
      //  EspressoIdlingResource.increment()

    }

    override fun onItemAtFrontLoaded(itemAtFront:ToDoItem) {
        super.onItemAtFrontLoaded(itemAtFront)

        disposable.add(useCase.getCount()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { result ->
                if (totalCount < result) totalCount = result
            })

    }

    override fun onItemAtEndLoaded(itemAtEnd: ToDoItem) {
        super.onItemAtEndLoaded(itemAtEnd)
        if (!isLoaded) {
            loadingFirstPage = totalCount <= 0
            fetchFromNetwork(totalCount, BuildConfig.PAGE_SIZE)
        }
    }

    fun fetchFromNetwork(offset: Int, limit: Int) {
        if (!networkUtils.isInternetAvailable()){
            updateState(State.NETWORK_ERROR)
            return
        }

        if (offset==0){
            updateState(State.LOADING)
        }else{
            updateState(State.PAGE_LOADING)
        }

        disposable.add(

            useCase.fetchItemList(offset, limit)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result ->
                    when (result) {
                        is Result.Success -> {
                            success(result.response as List<ToDoItem>)
                        }
                        is Result.Failure -> {
                            error(result.throwable)
                        }
                    }
                }, { e -> error(e) })
        )
    }

    private fun success(list: List<ToDoItem>) {
        totalCount += list.size
        if (list.size < BuildConfig.PAGE_SIZE) {
            isLoaded = true
            updateState(State.LOADED)
        } else
            updateState(State.DONE)
       // EspressoIdlingResource.decrement()
    }

    private fun error(throwable: Throwable) {
        if (throwable is UnknownHostException) {
            updateState(State.NETWORK_ERROR)
        } else {
            updateState(State.ERROR)
        }
       // EspressoIdlingResource.decrement()
    }

    fun updateState(state: String) {
        this.state.postValue(state)
    }

    fun retry() {
        loadingFirstPage = true
        fetchFromNetwork(totalCount, BuildConfig.PAGE_SIZE)
    }

    fun onRefresh() {
        totalCount = 0
        isLoaded = false
        disposable.clear()
        onZeroItemsLoaded()
    }
}
