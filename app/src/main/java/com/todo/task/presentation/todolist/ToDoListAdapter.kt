package com.todo.task.presentation.todolist

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.VisibleForTesting
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.todo.task.databinding.ItemTodoListBinding
import com.todo.task.domain.entity.ToDoItem



const val VIEW_TYPE_ITEM = 0
const val VIEW_TYPE_LOADING = 1

class ToDoListAdapter () : ListAdapter<ToDoItem, RecyclerView.ViewHolder>(mDiffCallback) {

    override fun getItemViewType(position: Int): Int {
        return if (position < super.getItemCount())
            VIEW_TYPE_ITEM
        else VIEW_TYPE_LOADING


    }

    override fun getItemCount(): Int {
        return super.getItemCount()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        return ItemHolder(
            ItemTodoListBinding.inflate(
                LayoutInflater.from(parent.context)
                , parent, false))
    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ItemHolder).bind(getItem(position) as ToDoItem)

    }


    class ItemHolder(
        private val binding: ItemTodoListBinding
    ) :
        RecyclerView.ViewHolder(binding.root) {

        lateinit var todoItem: ToDoItem
        fun bind(todoItem: ToDoItem) {
            binding.item = todoItem
            this.todoItem = todoItem

        }
    }


    companion object {
        val mDiffCallback = object : DiffUtil.ItemCallback<ToDoItem>() {
            override fun areItemsTheSame(oldItem: ToDoItem, newItem: ToDoItem): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: ToDoItem, newItem: ToDoItem): Boolean {
                return oldItem == newItem
            }
        }
    }


    @VisibleForTesting
    fun getToDoItem(position: Int): ToDoItem? {
        return super.getItem(position)
    }


}