package com.todo.task.presentation.todolist

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData

import androidx.lifecycle.MutableLiveData
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.order.delivery.utils.NetworkUtils
import com.todo.task.BuildConfig
import com.todo.task.domain.entity.ToDoItem
import com.todo.task.domain.usecase.todo.ToDoUseCase
import com.todo.task.presentation.BaseViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.net.UnknownHostException
import javax.inject.Inject
import com.todo.task.data.Result
import com.todo.task.data.repository.State

class ToDoViewModel @Inject constructor(private val useCase: ToDoUseCase,private val networkUtils: NetworkUtils): BaseViewModel() {

    var itemList : LiveData<PagedList<ToDoItem>>
    var item = ObservableField<ToDoItem>()
    var noData = ObservableBoolean(false)
    var boundaryCallback: ToDoBoundaryCallback

    init {
        val config = PagedList.Config.Builder()
            .setPageSize(BuildConfig.PAGE_SIZE)
            .setInitialLoadSizeHint(BuildConfig.PAGE_SIZE)
            .setEnablePlaceholders(false)
            .build()
        boundaryCallback =
            ToDoBoundaryCallback(useCase, getDisposable(),networkUtils)
        itemList = LivePagedListBuilder(useCase.getItemList(), config)
            .setBoundaryCallback(boundaryCallback).build()
    }

    fun retry() = boundaryCallback.retry()

    fun onRefresh() {
        isLoading.set(true)
        boundaryCallback.onRefresh()
    }
}