package com.todo.task.data.remote

import com.todo.task.data.remote.response.ToDoResponse
import io.reactivex.Single
import retrofit2.http.GET


interface ToDoService {

    @GET("todos")
    fun getToDoItemList(): Single<List<ToDoResponse>>
}