package com.todo.task.data.mapper

import com.todo.task.data.remote.response.ToDoResponse
import com.todo.task.domain.entity.ToDoItem
import javax.inject.Inject

class ToDoDataMapper @Inject constructor() {

    fun map(responseList: List<ToDoResponse>): List<ToDoItem> {
        return responseList.map { (map(it)) }
    }

    private fun map(response: ToDoResponse): ToDoItem {
        return ToDoItem(
            id = response.id,
            userId = response.userId,
            title = response.title,
            completed = response.completed)
    }
}