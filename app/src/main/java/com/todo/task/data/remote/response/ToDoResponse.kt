package com.todo.task.data.remote.response

import com.google.gson.annotations.SerializedName

data class ToDoResponse(
    @SerializedName("id")
    val id : Int,
    @SerializedName("userId")
    val userId : Int,
    @SerializedName("title")
    val title : String,
    @SerializedName("completed")
    val completed : Boolean
)