package com.todo.task.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.todo.task.data.local.dao.ToDoDao
import com.todo.task.domain.entity.ToDoItem

@Database(entities = [ToDoItem::class], version = 1,
          exportSchema = false)
abstract class ToDoDatabase : RoomDatabase() {

    abstract fun dao() : ToDoDao
}