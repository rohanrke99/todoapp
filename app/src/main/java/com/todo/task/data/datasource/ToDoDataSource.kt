package com.todo.task.data.datasource

import androidx.paging.DataSource
import com.todo.task.data.local.dao.ToDoDao
import com.todo.task.domain.datasource.BaseDataSource
import com.todo.task.domain.entity.ToDoItem
import io.reactivex.Single
import javax.inject.Inject

class ToDoDataSource @Inject constructor(private val dao: ToDoDao) : BaseDataSource<ToDoItem,Int> {

    override fun getEntities(): DataSource.Factory<Int, ToDoItem> {
        return dao.getToDoList()
    }

    override fun getById(id: Int): Single<ToDoItem> {
        return dao.getToDoItem(id)
    }

    override fun saveEntity(entity: ToDoItem) {
        dao.insertTodoItem(entity)
    }

    override fun saveEntityList(list: List<ToDoItem>) {
        dao.insertList(list)
    }

    override fun updateEntity(entity: ToDoItem) {
       dao.insertTodoItem(entity)
    }

    override fun deleteEntity(entity: ToDoItem) {
        dao.deleteTodo(entity.id)
    }

    override fun deleteAll() {
        dao.deleteAll()
    }

    override fun count(): Single<Int> {
        return dao.getCount()
    }
}