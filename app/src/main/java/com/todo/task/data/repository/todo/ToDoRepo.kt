package com.todo.task.data.repository.todo

import androidx.paging.DataSource
import com.todo.task.data.mapper.ToDoDataMapper
import com.todo.task.data.remote.ToDoService
import com.todo.task.domain.datasource.BaseDataSource
import com.todo.task.domain.entity.ToDoItem
import com.todo.task.domain.repository.BaseRepo
import io.reactivex.Single


class ToDoRepo constructor(
    private val service: ToDoService,
    private val mapper: ToDoDataMapper,
    private val dataSource: BaseDataSource<ToDoItem,Int>
    ) : BaseRepo<ToDoItem, Int> {

    override fun get(offset: Int, limit: Int): Single<List<ToDoItem>> {
        return service.getToDoItemList()
            .map {
                val list: List<ToDoItem> = mapper.map(it)
                if (offset == 0) {
                    dataSource.deleteAll()
                }
                dataSource.saveEntityList(list)
                return@map list
            }
    }

    override fun get(): DataSource.Factory<Int, ToDoItem> {
        return dataSource.getEntities()
    }

    override fun getById(id: Int): Single<ToDoItem> {
        return dataSource.getById(id)
    }

    override fun save(entity: ToDoItem) {
        dataSource.saveEntity(entity)
    }

    override fun save(list: List<ToDoItem>) {
        dataSource.saveEntityList(list)
    }

    override fun update(entity: ToDoItem) {
        dataSource.updateEntity(entity)
    }

    override fun delete(entity: ToDoItem) {
        dataSource.deleteEntity(entity)
    }

    override fun deleteAll() {
        dataSource.deleteAll()
    }

    override fun count(): Single<Int> {
        return dataSource.count()
    }
}