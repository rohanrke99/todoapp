package com.todo.task.data.local.dao

import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.todo.task.domain.entity.ToDoItem
import io.reactivex.Single

@Dao
interface ToDoDao {

    @Query("SELECT * FROM todo_item ORDER BY id ASC")
    fun getToDoList():  DataSource.Factory<Int, ToDoItem>

    @Query("SELECT * FROM todo_item WHERE id= :id")
    fun getToDoItem(id: Int): Single<ToDoItem>

    @Query("SELECT count(*) FROM todo_item")
    fun getCount(): Single<Int>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertTodoItem(toDoItem : ToDoItem)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertList(toDoItemList: List<ToDoItem>)

    @Query("DELETE FROM todo_item WHERE id= :id")
    fun deleteTodo(id: Int)

    @Query("DELETE FROM todo_item")
    fun deleteAll()
}