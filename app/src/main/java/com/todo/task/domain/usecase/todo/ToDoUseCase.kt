package com.todo.task.domain.usecase.todo

import androidx.paging.DataSource
import com.todo.task.data.Result
import com.todo.task.domain.entity.ToDoItem
import com.todo.task.domain.repository.BaseRepo
import com.todo.task.domain.usecase.BaseUseCase
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject

class ToDoUseCase @Inject constructor(private val repo: BaseRepo<ToDoItem, Int>):
    BaseUseCase<ToDoItem,Int> {

    override fun fetchItemList(offset: Int, limit: Int): Observable<Result> {
        return repo.get(offset, limit).toObservable()
            .map { Result.Success(it) as Result }
            .onErrorReturn { Result.Failure(it) }
            .startWith(Result.Loading)
    }

    override fun getItemList(): DataSource.Factory<Int, ToDoItem> {
        return repo.get()
    }

    override fun getSingleItem(id: Int): Single<ToDoItem> {

        return repo.getById(id)
    }

    override fun getCount(): Single<Int> {
        return repo.count()
    }

    override fun removeAll() {
        repo.deleteAll()
    }
}