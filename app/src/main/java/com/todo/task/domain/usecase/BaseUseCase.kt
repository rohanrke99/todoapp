package com.todo.task.domain.usecase


import androidx.paging.DataSource
import com.todo.task.data.Result
import io.reactivex.Observable
import io.reactivex.Single

interface BaseUseCase<T, K> {

    fun fetchItemList(offset: Int, limit: Int): Observable<Result>

    fun getItemList(): DataSource.Factory<K, T>

    fun getSingleItem(id: K): Single<T>

    fun getCount(): Single<Int>

    fun removeAll()
}