package com.todo.task.domain.entity

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(tableName = "todo_item",
    indices  = [Index(value = ["userId"], unique = false)])
data class ToDoItem(
     @PrimaryKey
     val id : Int,
     val userId : Int,
     val title : String,
     val completed : Boolean
)

