package com.todo.task.utils


interface BottomDialogListener {

    fun onDialogClickListener(retry: Boolean)
}