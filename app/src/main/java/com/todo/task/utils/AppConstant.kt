package com.todo.task.utils

import androidx.annotation.IntDef
import com.todo.task.utils.BottomDialogType.Companion.ALL_DATA_FETCHED
import com.todo.task.utils.BottomDialogType.Companion.ERROR_IN_FETCHING
import com.todo.task.BuildConfig
import okhttp3.logging.HttpLoggingInterceptor


val sHTTP_LOG_LEVEL = if (BuildConfig.DEBUG)
    HttpLoggingInterceptor.Level.BODY
else
    HttpLoggingInterceptor.Level.NONE

@IntDef(
    ALL_DATA_FETCHED,
    ERROR_IN_FETCHING
)

@kotlin.annotation.Retention(AnnotationRetention.SOURCE)
annotation class BottomDialogType {
    companion object {
        const val ALL_DATA_FETCHED = 1
        const val ERROR_IN_FETCHING = 2
    }
}

