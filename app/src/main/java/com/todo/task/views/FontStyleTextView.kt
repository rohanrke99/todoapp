package com.todo.task.views

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatTextView
import com.todo.task.R

import java.util.*


class FontStyleTextView : AppCompatTextView {

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        setCustomFont(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        setCustomFont(context, attrs)
    }

    private fun setCustomFont(ctx: Context, attrs: AttributeSet) {
        if (typefaces == null)
            typefaces = HashMap()

        val a = ctx.obtainStyledAttributes(attrs, R.styleable.FontStyleTextView)
        val customFont: String?
        customFont = if (a.hasValue(R.styleable.FontStyleTextView_customFont))
            a.getString(R.styleable.FontStyleTextView_customFont)
        else
            ctx.resources.getString(R.string.font_default)
        setCustomFont(ctx, customFont!!)

        includeFontPadding = false

        a.recycle()
    }

    private fun setCustomFont(ctx: Context, path: String) {
        val typeface: Typeface?
        if (typefaces!!.containsKey(path)) {
            typeface = typefaces!![path]
        } else {
            typeface = Typeface.createFromAsset(ctx.assets, path)
            typefaces!![path] = typeface!!
        }
        setTypeface(typeface)
    }

    companion object {
        /*
     * Caches typefaces based on their file path and name, so that they don't have to be created
     * every time when they are referenced.
     */
        private var typefaces: MutableMap<String, Typeface>? = null
    }


}
