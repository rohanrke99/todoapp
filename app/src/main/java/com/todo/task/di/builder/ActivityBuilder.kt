package com.todo.task.di.builder


import com.todo.task.presentation.todolist.ToDoListActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector
    internal abstract fun contributeToDoListActivity(): ToDoListActivity
}


