package com.todo.task.di.module


import com.todo.task.data.datasource.ToDoDataSource
import com.todo.task.data.local.dao.ToDoDao
import com.todo.task.data.mapper.ToDoDataMapper
import com.todo.task.data.remote.ToDoService
import com.todo.task.data.repository.todo.ToDoRepo
import com.todo.task.domain.datasource.BaseDataSource
import com.todo.task.domain.entity.ToDoItem
import com.todo.task.domain.repository.BaseRepo
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class RepoModule {

    @Provides
    @Singleton
    fun provideToDoDataSource(
        dao: ToDoDao
    ): BaseDataSource<ToDoItem, Int> {
        return ToDoDataSource(dao)
    }

    @Provides
    @Singleton
    fun provideToDoRepository(
        service: ToDoService,
        mapper: ToDoDataMapper,
        dataSource: ToDoDataSource
    ): BaseRepo<ToDoItem, Int> {
        return ToDoRepo(service, mapper, dataSource)
    }
}