package com.todo.task.di.module


import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.order.delivery.di.AppViewModelFactory
import com.todo.task.presentation.todolist.ToDoViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    internal abstract fun bindViewModelFactory(factory: AppViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(ToDoViewModel::class)
    internal abstract fun toDoListViewModel(viewModel: ToDoViewModel): ViewModel
}
