package com.todo.task.di.module

import android.app.Application
import androidx.room.Room
import com.todo.task.BuildConfig
import com.todo.task.data.local.ToDoDatabase
import com.todo.task.data.local.dao.ToDoDao

import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DatabaseModule {
    @Singleton
    @Provides
    fun provideDatabase(application: Application): ToDoDatabase {
        return Room.databaseBuilder(
            application, ToDoDatabase::class.java,
            BuildConfig.DATABASE_NAME
        ).fallbackToDestructiveMigration().build()
    }

    @Provides
    fun provideToDoDao(database: ToDoDatabase): ToDoDao {
        return database.dao()
    }
}