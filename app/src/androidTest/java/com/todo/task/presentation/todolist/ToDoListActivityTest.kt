package com.todo.task.presentation.todolist

import android.content.Intent
import androidx.recyclerview.widget.RecyclerView
import androidx.room.Room
import androidx.test.espresso.IdlingRegistry
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.todo.task.R
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.contrib.RecyclerViewActions.actionOnHolderItem
import androidx.test.espresso.contrib.RecyclerViewActions.scrollToHolder
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.rule.ActivityTestRule
import com.squareup.okhttp.mockwebserver.MockResponse
import com.squareup.okhttp.mockwebserver.MockWebServer
import com.todo.task.data.local.ToDoDatabase
import com.todo.task.utils.CustomViewMatchers
import com.todo.task.utils.TestUtil
import com.todo.task.views.EspressoIdlingResource
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class ToDoListActivityTest {


    private lateinit var todoDb: ToDoDatabase


    @Rule
    @JvmField
    val activityRule = ActivityTestRule(ToDoListActivity::class.java, true, true)


    private lateinit var mockServer: MockWebServer

    @Before
    fun setUp() {

        IdlingRegistry.getInstance().register(EspressoIdlingResource.getIdlingResource())

        todoDb = Room.inMemoryDatabaseBuilder(
            InstrumentationRegistry.getInstrumentation()
                .context, ToDoDatabase::class.java
        ).build()
        todoDb.dao().deleteAll()

        mockServer = MockWebServer()
        mockServer.play(TestUtil.MOCK_PORT)


    }

    @Test
    fun mockDataSuccessTest() {
        initTest()
        mockServer.enqueue(TestUtil.createResponse("json/sample_data.json"))




        assert(activityRule.activity.adapter.itemCount == 2)
        assert(
            activityRule.activity.adapter.getToDoItem(0)?.title.equals(
                "delectus aut autem"
            )
        )

        val matcher = CustomViewMatchers.withToDoTitle(
            "1delectus aut autem"
        )


        onView((withId(R.id.recycler_view))).perform(scrollToHolder(matcher), actionOnHolderItem(matcher, click()))
    }

    @Test
    fun mockApiFailTest() {
        mockServer.enqueue(MockResponse().setResponseCode(400).setBody("Internal server error"))

        assert(activityRule.activity.adapter.itemCount == 0)

        onView(withText(activityRule.activity.getString(R.string.todo_list_error_message)))
            .withFailureHandler { _, _ -> }.check(matches(isDisplayed()))
    }

    @Test
    fun viewTest() {
        initTest()

        Espresso.onView(withId(R.id.pb_frag_load_more_progress))
            .check(matches(withEffectiveVisibility(Visibility.GONE)))
        Espresso.onView(withId(R.id.recycler_view))
            .check(matches(withEffectiveVisibility(Visibility.VISIBLE)))

        onView(withId(R.id.recycler_view))
            .perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(20))
    }

    @Test
    fun clickTest() {
        initTest()

        onView(withId(R.id.recycler_view))
            .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, click()))

    }


    private fun initTest() {
        mockServer.enqueue(TestUtil.createResponse("json/sample_data.json"))

        activityRule.launchActivity(Intent())
        onView(withText("OK"))
            .withFailureHandler { _, _ -> }.check(matches(isDisplayed())).perform(click())
    }

    @After
    fun tearUp() {
        IdlingRegistry.getInstance().unregister(EspressoIdlingResource.getIdlingResource())
        mockServer.shutdown()

    }
    /*@Test
    fun pullToRefresh_shouldPass() {
        initTest()
        onView(withId(R.id.swipe_refresh)).perform(swipeDown())
    }
    */
}