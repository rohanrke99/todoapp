package com.todo.task.utils

import androidx.test.platform.app.InstrumentationRegistry
import com.squareup.okhttp.mockwebserver.MockResponse
import com.todo.task.domain.entity.ToDoItem

object TestUtil {

    fun createToDoItem(): ToDoItem {

        return ToDoItem(
            userId = 1,id = 1,
            title = "delectus aut autem",
            completed = false)
    }

    fun createToDoItemList(): List<ToDoItem> {
        val list = ArrayList<ToDoItem>()
        for (i in 0 until 10) {
            list.add(ToDoItem(
                userId = 1,id = 1,
                title = "delectus aut autem",
                completed = false))
        }
        return list
    }

    const val MOCK_PORT: Int = 8888

    private fun loadJson(path: String): String {
        val context = InstrumentationRegistry.getInstrumentation().context
        val stream = context.resources.assets.open(path)
        val reader = stream.bufferedReader()
        val stringBuilder = StringBuilder()
        reader.lines().forEach {
            stringBuilder.append(it)
        }
        return stringBuilder.toString()
    }

    fun createResponse(path: String): MockResponse = MockResponse()
        .setResponseCode(200)
        .setBody(loadJson(path))
}