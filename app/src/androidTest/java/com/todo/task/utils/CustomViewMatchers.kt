package com.todo.task.utils

import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.matcher.BoundedMatcher
import com.todo.task.presentation.todolist.ToDoListAdapter
import org.hamcrest.Matcher

object CustomViewMatchers {

    fun withToDoTitle(title: String): Matcher<RecyclerView.ViewHolder> {
        return object :
            BoundedMatcher<RecyclerView.ViewHolder, ToDoListAdapter.ItemHolder>(
                ToDoListAdapter.ItemHolder::class.java
            ) {
            override fun matchesSafely(item: ToDoListAdapter.ItemHolder): Boolean {
                return (item.todoItem.id.toString().plus(
                    item.todoItem.title
                ) == title)
            }

            override fun describeTo(description: org.hamcrest.Description?) {
                description?.appendText("view holder with description: $title")
            }
        }
    }
}