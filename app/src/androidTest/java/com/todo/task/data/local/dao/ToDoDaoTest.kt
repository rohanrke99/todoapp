package com.todo.task.data.local.dao

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.todo.task.data.local.ToDoDbTest
import com.todo.task.utils.TestUtil
import org.hamcrest.CoreMatchers
import org.hamcrest.MatcherAssert
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class ToDoDaoTest : ToDoDbTest(){

    @Test
    fun insertAndReadTest() {

        val todoItem = TestUtil.createToDoItem()

        db.dao().deleteAll()

        db.dao().getCount().test()?.assertValue(0)

        db.dao().insertTodoItem(todoItem)

        db.dao().getCount().test()?.assertValue(1)

        val item = (db.dao().getToDoItem(1))

        MatcherAssert.assertThat(item, CoreMatchers.notNullValue())
        item.test().assertValue(todoItem)

        MatcherAssert.assertThat(item.blockingGet(), CoreMatchers.notNullValue())
        MatcherAssert.assertThat(item.blockingGet().title, CoreMatchers.notNullValue())
        MatcherAssert.assertThat(item.blockingGet().id, CoreMatchers.notNullValue())

    }

    @Test
    fun insertAllAndReadTest() {
        val list = TestUtil.createToDoItemList()
        db.dao().deleteAll()

        db.dao().getCount().test()?.assertValue(0)

        db.dao().insertList(list)

        db.dao().getCount().test()?.assertValue(1)

    }
}