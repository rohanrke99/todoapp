package com.todo.task.di.component

import android.app.Application
import com.todo.task.ToDoApp
import com.todo.task.di.builder.ActivityBuilder
import com.todo.task.di.module.*
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [AndroidInjectionModule::class,
        AppModule::class, ViewModelModule::class,
        ActivityBuilder::class, DatabaseModule::class,
        RepoModule::class, NetworkModule::class]
)
interface TestAppComponent {

    fun inject(app: ToDoApp)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }
}